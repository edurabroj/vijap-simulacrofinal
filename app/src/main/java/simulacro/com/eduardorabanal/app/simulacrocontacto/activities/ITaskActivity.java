package simulacro.com.eduardorabanal.app.simulacrocontacto.activities;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.widget.ListView;

import org.json.JSONException;

import java.util.List;

import simulacro.com.eduardorabanal.app.simulacrocontacto.R;

/**
 * Created by USER on 28/06/2017.
 */
public interface ITaskActivity{
    public void UsarDatos(String respuesta) throws JSONException;
    public AppCompatActivity GetActivity();
    public Context GetContext();
}
