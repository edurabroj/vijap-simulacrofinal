package simulacro.com.eduardorabanal.app.simulacrocontacto.activities;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import simulacro.com.eduardorabanal.app.simulacrocontacto.R;
import simulacro.com.eduardorabanal.app.simulacrocontacto.adapters.ContactoAdapter;
import simulacro.com.eduardorabanal.app.simulacrocontacto.models.Contacto;
import simulacro.com.eduardorabanal.app.simulacrocontacto.tasks.JsonTask;

public class ListaActivity extends AppCompatActivity implements ITaskActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista);

        JsonTask lt = new JsonTask(this,"http://192.168.0.7/simulacro","GET","");
        lt.execute();
    }

    @Override
    public void UsarDatos(String respuesta) throws JSONException {
//        Log.i("RESPUESTA: ", respuesta);

        ListView lv = (ListView) findViewById(R.id.lv_contacto);
        List<Contacto> datos=GetDatosFromJson(respuesta);

        ContactoAdapter adaptador =
                new ContactoAdapter(
                        this,
                        R.layout.item_contacto,
                        datos
                );

        lv.setAdapter(adaptador);
    }

    @Override
    public AppCompatActivity GetActivity() {
        return this;
    }

    @Override
    public Context GetContext() {
        return getApplicationContext();
    }

    private List<Contacto> GetDatosFromJson(String respuesta) throws JSONException {
        List<Contacto> datos = new ArrayList<>();
        JSONArray array = new JSONArray(respuesta);

        for (int i=0; i<array.length(); i++){
            JSONObject item = array.getJSONObject(i);
            Contacto contacto = new Contacto();
            contacto.setContactoId(item.getInt("ContactoId"));
            contacto.setNombre(item.getString("Nombre"));
            contacto.setTelefono(item.getString("Telefono"));
            contacto.setImgUrl(item.getString("ImgUrl"));
            contacto.setEsFavorito(item.getBoolean("EsFavorito"));
            datos.add(contacto);
        }

        return datos;
    }
}
