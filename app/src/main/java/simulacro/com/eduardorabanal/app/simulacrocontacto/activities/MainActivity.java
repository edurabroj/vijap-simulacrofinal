package simulacro.com.eduardorabanal.app.simulacrocontacto.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import simulacro.com.eduardorabanal.app.simulacrocontacto.R;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        Button btnTodos = (Button) findViewById(R.id.btnTodos);
        Button btnFavoritos = (Button) findViewById(R.id.btnFavoritos);


        assert btnTodos != null;
        btnTodos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),ListaActivity.class);
                intent.putExtra("soloFavoritos", false);
                startActivity(intent);
            }
        });


        assert btnFavoritos != null;
        btnFavoritos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),ListaActivity.class);
                intent.putExtra("soloFavoritos", true);
                startActivity(intent);
            }
        });
    }
}
