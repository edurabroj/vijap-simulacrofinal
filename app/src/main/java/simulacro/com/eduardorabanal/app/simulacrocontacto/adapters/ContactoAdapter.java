package simulacro.com.eduardorabanal.app.simulacrocontacto.adapters;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.List;

import simulacro.com.eduardorabanal.app.simulacrocontacto.R;
import simulacro.com.eduardorabanal.app.simulacrocontacto.activities.ITaskActivity;
import simulacro.com.eduardorabanal.app.simulacrocontacto.activities.ListaActivity;
import simulacro.com.eduardorabanal.app.simulacrocontacto.models.Contacto;
import simulacro.com.eduardorabanal.app.simulacrocontacto.tasks.JsonTask;

/**
 * Created by USER on 28/06/2017.
 */
public class ContactoAdapter  extends BaseAdapter {
    ITaskActivity _activity;
    int _item_layout;
    List<Contacto> _datos;

    public ContactoAdapter(
            ITaskActivity activity,
            int item_layout,
            List<Contacto> datos) {
        _activity = activity;
        _item_layout = item_layout;
        _datos = datos;
    }

    @Override
    public int getCount() {
        return _datos.size();
    }

    @Override
    public Contacto getItem(int position) {
        return _datos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater vi = _activity.GetActivity().getLayoutInflater();
            convertView = vi.inflate(_item_layout, parent, false);
        }

        final Contacto obj = getItem(position);

        TextView tv_nombre = (TextView) convertView.findViewById(R.id.tv_nombre);
        tv_nombre.setText(obj.getNombre());

        TextView tv_telefono = (TextView) convertView.findViewById(R.id.tv_telefono);
        tv_telefono.setText(obj.getTelefono());

        CheckBox esFavorito = (CheckBox) convertView.findViewById(R.id.esFavorito);
        esFavorito.setChecked(obj.getEsFavorito());

        esFavorito.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JsonTask UpdateTask = new JsonTask(_activity ,"http://192.168.0.7/simulacro/home/update","POST","id=1");
                UpdateTask.execute();
                _activity.GetActivity().startActivity(new Intent(_activity.GetContext(), ListaActivity.class));
            }
        });

        return convertView;
    }
}