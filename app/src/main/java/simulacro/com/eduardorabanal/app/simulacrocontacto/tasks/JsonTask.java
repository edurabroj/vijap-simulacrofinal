package simulacro.com.eduardorabanal.app.simulacrocontacto.tasks;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONException;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import simulacro.com.eduardorabanal.app.simulacrocontacto.activities.ITaskActivity;

/**
 * Created by USER on 28/06/2017.
 */
public class JsonTask extends AsyncTask<String, Void, String> {
    private ITaskActivity activity;
    private String uri;
    private String method;
    private String postData;

    public ITaskActivity getActivity() {
        return activity;
    }

    public void setActivity(ITaskActivity activity) {
        this.activity = activity;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getPostData() {
        return postData;
    }

    public void setPostData(String postData) {
        this.postData = postData;
    }

    public JsonTask() {
    }

    public JsonTask(ITaskActivity activity, String uri, String method, String postData) {
        this.activity = activity;
        this.uri = uri;
        this.method = method;
        this.postData = postData;
    }

    @Override
    protected void onPreExecute() {
        Toast.makeText(activity.GetContext(),"Iniciando",Toast.LENGTH_SHORT).show();
    }

    @Override
    protected String doInBackground(String... params) {
        //0 o más parametros
        HttpURLConnection connection = null;
        String respuesta = null;

        if(method=="GET")
        {
            try {
                //configurar
                URL url = new URL(uri);
                connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod(method);

                //obtener datos
                InputStream is = connection.getInputStream();
                BufferedReader r = new BufferedReader(new InputStreamReader(is));

                respuesta = r.readLine();
                r.close();
                is.close();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                connection.disconnect();
            }
        }else if(method=="POST"){
            try {
                //configurar
                URL url = new URL(uri);
                connection= (HttpURLConnection)url.openConnection();
                connection.setRequestMethod(method);

                connection.setDoInput(true);
                connection.setDoOutput(true);

                //obtener datos
                OutputStream s=connection.getOutputStream();
                BufferedWriter b = new BufferedWriter(new OutputStreamWriter(s,"UTF-8"));

                Log.i("PostData", postData);
                b.write(postData);
                b.flush();
                b.close();
                s.close();

                int code = connection.getResponseCode();
                respuesta = connection.getResponseMessage();

                Log.i("RESPONDE_CODE", String.valueOf(code));
                Log.i("RESPONDE_MESSAGE", respuesta);

                connection.connect();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                connection.disconnect();
            }
        }
        return respuesta;
    }

    @Override
    protected void onPostExecute(String s) {
        try {
            activity.UsarDatos(s);
            Toast.makeText(activity.GetContext(),"Terminado",Toast.LENGTH_SHORT).show();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}